# Welcome to your CDK TypeScript project

This is a blank project for CDK development with TypeScript.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Setup

- `npm install -g aws-cdk-local aws-cdk`
- `pip install awscli-local awscli` ou `brew install awscli-local awscli`
- `docker-compose up -d` Il faut créer un fichier .env avec la varraible `Be2QUrp0AW` avant de lancer la commande
- `cdklocal bootstrap`
- `cdklocal deploy`

## Useful commands

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `npm run test` perform the jest unit tests
- `cdk deploy` deploy this stack to your default AWS account/region
- `cdk diff` compare deployed stack with current state
- `cdk synth` emits the synthesized CloudFormation template

## Useful commands for docker

- `docker-compose down --remove-orphans`
- `docker-compose up -d`

## Useful commands for localstack

- `cdklocal bootstrap`
- `cdklocal deploy`
- `aws [command] --endpoint-url http://localhost:4566/`
- `awslocal`
- `awslocal lambda list-functions`
- `awslocal lambda invoke --function-name [FunctionName] --cli-binary-format raw-in-base64-out response.json`
- `awslocal s3api list-buckets`
- `awslocal s3api put-object --bucket cdkawsstack-mybucketf68f3ff0-dcd554ef --key response.json --body response.json`
