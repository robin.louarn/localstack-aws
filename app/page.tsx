"use client";

import {
  Box,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Container,
  Flex,
  Heading,
  Input,
  Text,
  VStack,
} from "@chakra-ui/react";
import { HeadBlobResult, PutBlobResult } from "@vercel/blob";
import { upload } from "@vercel/blob/client";
import Image from "next/image";
import React, { useRef, useState } from "react";
import { useForm, useFieldArray, SubmitHandler } from "react-hook-form";
import { getMetadata } from "./actions";

type FormType = {
  files: Array<{ file: File; progress: number }>;
};

export default function Home() {
  const {
    control,
    reset,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<FormType>();
  const { fields, append, remove } = useFieldArray({
    control,
    name: "files",
  });

  const [images, setImages] = useState<
    Array<{ put: PutBlobResult; head: HeadBlobResult }>
  >([]);
  const inputFile = useRef<HTMLInputElement>(null);

  const onSubmit: SubmitHandler<FormType> = async () => {
    const images = await Promise.all(
      fields.map(async ({ file }) => {
        const put = await upload(file.name, file, {
          access: "public",
          handleUploadUrl: "/api/avatar/upload",
        });
        const head = await getMetadata(put.url);
        return { put, head };
      })
    );
    setImages(images);
    reset({
      files: [],
    });
    if (inputFile.current) inputFile.current.value = "";
  };

  return (
    <VStack>
      <Container maxW="md" color="white">
        <Card size="sm">
          <CardHeader>
            <Heading size="md">Importer des fichiers</Heading>
          </CardHeader>
          <CardBody>
            <Input
              type="file"
              multiple
              ref={inputFile}
              onChange={(e) => {
                const files = e.target.files;
                if (files)
                  append(
                    Array.from(files)
                      // filtrer les files null
                      .filter((file) => !!file)
                      // filtrer les doublons
                      .filter(({ name: name1 }) => {
                        const filtre = !fields.some(
                          ({ file: { name: name2 } }) => name1 === name2
                        );
                        if (!filtre) console.log(`${name1} est en doublon`);
                        return filtre;
                      })
                      .map((file) => ({ file, progress: 0 }))
                  );
              }}
            />
            <VStack as="form" onSubmit={handleSubmit(onSubmit)} p={2}>
              {fields.map(({ id, file: { name } }, index) => (
                <Flex key={id} w="full" flexDir="column" gap={1}>
                  <Flex justifyContent="space-between" alignItems="center">
                    <Text>{name}</Text>
                    <Button type="button" onClick={() => remove(index)}>
                      Remove
                    </Button>
                  </Flex>
                </Flex>
              ))}
              <Box>
                <Button type="submit" isLoading={isSubmitting}>
                  Submit
                </Button>
              </Box>
            </VStack>
          </CardBody>
          <CardFooter>
            {images.map(({ put: { url, pathname } }) => (
              <Image
                key={pathname}
                src={url}
                alt={pathname}
                width={100}
                height={100}
              />
            ))}
          </CardFooter>
        </Card>
      </Container>
    </VStack>
  );
}
