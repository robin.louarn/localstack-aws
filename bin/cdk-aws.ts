#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "aws-cdk-lib";
import { BucketStack } from "@/lib/bucket-stack";
import { DataBaseStack } from "@/lib/database-stack";

const app = new cdk.App();
new BucketStack(app, "BucketStack", {});
new DataBaseStack(app, "DataBaseStack", {
  env: {
    region: "eu-west-1",
  },
});
