import { App, CfnOutput, Stack, StackProps } from "aws-cdk-lib";
import { Code, Function, Runtime } from "aws-cdk-lib/aws-lambda";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import { Bucket, EventType } from "aws-cdk-lib/aws-s3";
import { LambdaDestination } from "aws-cdk-lib/aws-s3-notifications";

export class BucketStack extends Stack {
  constructor(scope: App, id: string, props?: StackProps) {
    super(scope, id, props);

    const myBucket = new Bucket(this, "MyBucket");

    const helloFunction = new NodejsFunction(this, "HelloFunction", {
      runtime: Runtime.NODEJS_18_X,
      handler: "handler",
      entry: "lib/lambda/hello.ts",
      bundling: {
        tsconfig: "tsconfig.lambda.json",
      },
    });

    myBucket.grantRead(helloFunction);

    myBucket.addEventNotification(
      EventType.OBJECT_CREATED,
      new LambdaDestination(helloFunction)
    );

    new CfnOutput(this, "myBucketName", {
      value: myBucket.bucketName,
    });
    new CfnOutput(this, "helloFunctionName", {
      value: helloFunction.functionName,
    });
  }
}
