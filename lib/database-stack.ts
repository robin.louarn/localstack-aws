import { App, CfnOutput, Stack, StackProps } from "aws-cdk-lib";
import { InstanceType, Vpc } from "aws-cdk-lib/aws-ec2";
import {
  AuroraPostgresEngineVersion,
  Credentials,
  DatabaseCluster,
  DatabaseClusterEngine,
} from "aws-cdk-lib/aws-rds";
import { Secret } from "aws-cdk-lib/aws-secretsmanager";

export class DataBaseStack extends Stack {
  constructor(scope: App, id: string, props: StackProps) {
    super(scope, id, props);

    const vpc = new Vpc(this, "TheVPC", {
      natGateways: 1,
    });

    // Create a secret in Secrets Manager for the master user credentials with an auto-generated password
    const masterUserSecret = new Secret(this, "DBCredentialsSecret", {
      secretName: "credentials",
      generateSecretString: {
        secretStringTemplate: JSON.stringify({
          username: "postgres",
        }),
        excludePunctuation: true,
        includeSpace: false,
        generateStringKey: "password",
      },
    });

    const cluster = new DatabaseCluster(this, "MyAuroraCluster", {
      engine: DatabaseClusterEngine.auroraPostgres({
        version: AuroraPostgresEngineVersion.VER_15_3,
      }),
      credentials: Credentials.fromSecret(masterUserSecret),
      instances: 1,
      defaultDatabaseName: "defaultDb",
      instanceProps: {
        instanceType: new InstanceType("db.t3.large"),
        vpc,
      },
    });

    new CfnOutput(this, "cluster", {
      value: cluster.clusterEndpoint.socketAddress,
    });
    new CfnOutput(this, "masterUserSecret", {
      value: masterUserSecret.secretArn,
    });
  }
}
